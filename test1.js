
// Create a function which return an array by formula i = p x r x t ÷ 100. 
// Use map, filter functions
// Remove negative numbers from array
// Answer is in the format of an array of integer round up to 2 decimal places. Eg. [1.01, 2.23]
// Create unit test using assertion.
const assert= require('assert');
function mapCondition(obj)
{
  return ((obj.p*obj.r*obj.t)/100).toFixed(2);
}
function filterCondition(num)
{
  return num>0;
}
function arrayFunctions(arr)
{
  var newArray=arr.map(mapCondition);
  var posArray=newArray.filter(filterCondition);
  return posArray;
}

//TestCase1
//finalAnswerCheck
//console.log(arrayFunctions(test));
const test = [
  {
    "p": 73,
    "r": 98,
    "t": 11
  },
  {
    "p": 25,
    "r": 37,
    "t": 77
  },
  {
    "p": 34,
    "r": -44,
    "t": 69
  },
  {
    "p": 86,
    "r": 25,
    "t": 27
  },
  {
    "p": 27,
    "r": -38,
    "t": 11
  },
  {
    "p": 4,
    "r": 11,
    "t": 66
  }
 ]
var testing=[ '786.94', '712.25', '580.50', '29.04' ]
assert.deepEqual(arrayFunctions(test),testing)

//TestCase2 (more negatives)


const test2 = [
  {
    "p": 73,
    "r": 98,
    "t": 11
  },
  {
    "p": 25,
    "r": 37,
    "t": 77
  },
  {
    "p": -34,
    "r": -44,
    "t": 69
  },
  {
    "p": -86,
    "r": -25,
    "t": 27
  },
  {
    "p": 27,
    "r": -38,
    "t": 11
  },
  {
    "p": 4,
    "r": 11,
    "t": 66
  }
 ]
var testing2=[ '786.94', '712.25', '1032.24', '580.50', '29.04' ]
//console.log(arrayFunctions(test2));
assert.deepEqual(arrayFunctions(test2),testing2)

//TestCase 3 (Only negatives)
const test3 = [
  {
    "p": -73,
    "r": -98,
    "t": -11
  },
  {
    "p": -25,
    "r": -37,
    "t": -77
  },
  {
    "p": -34,
    "r": -44,
    "t": -69
  },
  {
    "p": -86,
    "r": -25,
    "t": -27
  },
  {
    "p": -27,
    "r": -38,
    "t": -11
  },
  {
    "p": -4,
    "r": -11,
    "t": -66
  }
 ]
var testing3=[ ]
// console.log(arrayFunctions(test3));
assert.deepEqual(arrayFunctions(test3),testing3);



//TestCase4 (t removed)


const test4 = [
  {
    "p": 73,
    "r": 98,
    "t": 0
  },
  {
    "p": 25,
    "r": 37,
    "t": 0
  },
  {
    "p": -34,
    "r": -44,
    "t": 0
  },
  {
    "p": -86,
    "r": -25,
    "t": 0
  },
  {
    "p": 27,
    "r": -38,
    "t": 0
  },
  {
    "p": 4,
    "r": 11,
    "t": 0
  }
 ]
var testing4=[]
// console.log(arrayFunctions(test4));
assert.deepEqual(arrayFunctions(test4),testing4)

