const assert=require('assert');

function voterResults(arr) {
  const obj = {
    youngVotes: 0,

    youth: 0,
    mids: 0,
    oldVotes: 0,
    olds: 0,

  }
  const voted = {
    youngVoted: 0,
    youthVoted: 0,
    midsVoted: 0,
    oldVoted: 0,
    oldsVoted: 0

  }
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].age >= 18 && arr[i].age <= 25) {
      obj.youngVotes = obj.youngVotes + 1;
      if (arr[i].voted == true) {
        voted.youngVoted += 1;
      }
    }
    if (arr[i].age >= 26 && arr[i].age <= 35) {
      obj.youth = obj.youth + 1;
      if (arr[i].voted == true) {
        voted.youthVoted += 1;
      }
    }
    if (arr[i].age >= 36 && arr[i].age <= 55) {
      obj.mids = obj.mids + 1;
      if (arr[i].voted == true) {
        voted.midsVoted += 1;
      }
    }
  }
  return [obj,  voted]
}
// console.log(voterResults(voters)); // Returned value shown below:
/*
{
 youngVotes: 1,
 youth: 4,
 midVotes: 3,
 mids: 4,
 oldVotes: 3,
 olds: 4
}
*/
// Include how many of the potential voters were in the ages 18-25,
// how many from 26-35, how many from 36-55, and how many of each of those age
//ranges actually voted. 
// The resulting object containing this data should have 6 properties. 
// See the example output at the bottom.

// TestCase1 (Normal Check according to question)
var voters = [
  { name: 'Bob', age: 30, voted: true },
  { name: 'Jake', age: 32, voted: true },
  { name: 'Kate', age: 25, voted: false },
  { name: 'Sam', age: 20, voted: false },
  { name: 'Phil', age: 21, voted: true },
  { name: 'Ed', age: 55, voted: true },
  { name: 'Tami', age: 54, voted: true },
  { name: 'Mary', age: 31, voted: false },
  { name: 'Becky', age: 43, voted: false },
  { name: 'Joey', age: 41, voted: true },
  { name: 'Jeff', age: 30, voted: true },
  { name: 'Zack', age: 19, voted: false }
];

var testing1=[ { youngVotes: 4, youth: 4, mids: 4, oldVotes: 0, olds: 0 },
  { youngVoted: 1,
    youthVoted: 3,
    midsVoted: 3,
    oldVoted: 0,
    oldsVoted: 0 } ]

assert.deepEqual(voterResults(voters),testing1)

//testCase2

var voters2= [
  { name: 'Bob', age: 30, voted: true },
  { name: 'Jake', age: 32, voted: true },
  { name: 'Kate', age: 25, voted: false },
  { name: 'Sam', age: 45, voted: true },
  { name: 'Phil', age: 21, voted: true },
  { name: 'Ed', age: 55, voted: true },
  { name: 'Tami', age: 54, voted: true },
  { name: 'Mary', age: 31, voted: false },
  { name: 'Becky', age: 43, voted: true },
  { name: 'Joey', age: 41, voted: true },
  { name: 'Jeff', age: 30, voted: true },
  { name: 'Zack', age: 19, voted: false }
];

var testing2=[ { youngVotes: 3, youth: 4, mids: 5, oldVotes: 0, olds: 0 },
  { youngVoted: 1,
    youthVoted: 3,
    midsVoted: 5,
    oldVoted: 0,
    oldsVoted: 0 } ]
// console.log(voterResults(voters2))
assert.deepEqual(voterResults(voters2),testing2)



//testCase3(all false)

var voters3= [
  { name: 'Bob', age: 30, voted: false },
  { name: 'Jake', age: 32, voted: false },
  { name: 'Kate', age: 25, voted: false },
  { name: 'Sam', age: 45, voted: false },
  { name: 'Phil', age: 21, voted: false },
  { name: 'Ed', age: 55, voted: false },
  { name: 'Tami', age: 54, voted: false },
  { name: 'Mary', age: 31, voted: false },
  { name: 'Becky', age: 43, voted: false },
  { name: 'Joey', age: 41, voted: false },
  { name: 'Jeff', age: 30, voted: false },
  { name: 'Zack', age: 19, voted: false }
];

var testing3=[ { youngVotes: 3, youth: 4, mids: 5, oldVotes: 0, olds: 0 },
  { youngVoted: 0,
    youthVoted: 0,
    midsVoted: 0,
    oldVoted: 0,
    oldsVoted: 0 } ]
// console.log(voterResults(voters3))
 assert.deepEqual(voterResults(voters3),testing3)



